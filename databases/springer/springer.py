import pandas, requests, json
from .helper_functions_springer import mod_create_url_search
from api_keys import springer_api_key

# run search on Springer API
def springer_search_results(query, springer_api_key=springer_api_key):

    results_data = []                   # temporary list to store extracted data

    try:
        query = query.replace(" ", "+")     # search query (search keyword in all fields)
        records_num = 50                    # 50 max results based on documentation
        result_count = 0                    # results count
        results = []                        # list to store json_responses for result searches (allows to collect more than 50 results)

        # run search
        url = mod_create_url_search(query, records_num, 1,springer_api_key)
        response_API = requests.get(url)
        response_json = response_API.json()

        # get total results
        results_remaining = int(((response_json["result"])[0])["total"])

        # append first search results to json response list
        results.append(response_json)
        results_remaining -= records_num
        current_result_index = records_num + 1

        # if unaccounted results > 50 (limit per response on Springer API), run multiple searches
        while(results_remaining) > 0:
            # run search
            url = mod_create_url_search(query, records_num, current_result_index,springer_api_key)
            response_API = requests.get(url)
            response_json = response_API.json()

            # append to response list
            results.append(response_json)
            results_remaining -= records_num
            current_result_index += records_num
        
        # store desired fields
        try:
            for response in results:
                search_results = response["records"]
                try:
                    for article in search_results:

                        result_type = article["contentType"]

                        title = (article["title"])
                        year = str(article["publicationDate"])[0:4]

                        # author(s)
                        authors = []
                        if "creators" in article:
                            author_count = len(article["creators"])
                        
                            for i in range(author_count):
                                authors.append(((article["creators"])[i])["creator"])

                        source = article["publicationName"]
                        cited_by_count = "Unknown"
                        url = ((article["url"])[0])["value"]
                        pdf_file_url = "https://link-springer-com.du.idm.oclc.org/content/pdf/" + str(article["doi"])
                        abstract = article["abstract"]

                        results_data.append({
                            "database": "Springer",
                            "result_type": result_type,
                            "title": title,
                            "year": year,
                            "authors": authors,
                            "source": source,
                            "cited_by_count": cited_by_count,
                            "url": url,
                            "pdf_file_url": pdf_file_url,
                            "abstract": abstract,
                        })

                        result_count += 1
                            
                except TypeError as e:
                    pass

                except KeyError as e:
                    print("\t*** Springer KeyError: " + str(e) + " ***")
                    pass
        
        except None:
            pass

        print("\t" + str(result_count) + " matching Springer search results found... ")
        return results_data

    except requests.HTTPError as e:
        print("\t*** Springer APIError requests.HTTPError (Missing/Invalid Key): " + str(e) + " ***")
        return results_data
    except requests.exceptions.RequestException as e:
        print("\t*** Springer APIError requests.exceptions.RequestException (Missing/Invalid Key): " + str(e) + " ***")
        return results_data
    except Exception as e:
        print("\t*** Springer APIError (Missing/Invalid Key): " + str(e) + " ***")
        return results_data
    

def main():
    query = input("\nPlease enter your search query:\t")
    pandas.DataFrame(data=springer_search_results(query)).to_csv("springer.csv", encoding="utf-8", index=False)
    print("\tSpringer search results saved!")


if __name__ == "__main__":
    main()