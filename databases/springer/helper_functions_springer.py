import sys                                  # to import API keys

# get API key
api_key_path = (str(sys.path[0])).rsplit("\\",1)[0].rsplit("\\",1)[0]
sys.path.insert(0, api_key_path)

# Creates the search url to query Springer API by combining the standard url and various search parameters
def mod_create_url_search(query, records_num, start, springer_api_key):

        url = "https://api.springernature.com/metadata/json?"   # base url
        url += "api_key=" + springer_api_key                    # add key
        url += "&q=" + query                                    # add query
        url += "&s=" + str(start)                               # start collecting the nth result (parameter for function)
        url += "&p=" + str(records_num)                         # add max results

        return url