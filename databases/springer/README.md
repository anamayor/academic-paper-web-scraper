# Springer Module
This module extracts search results and their respective metadata from Springer's search result pages using Springer's API tool.

## Springer API & Database Overview
Springer Nature is a leading global scientific publisher of books and journals, delivering quality content through innovative information products and services. It publishes close to 500 academic and professional society journals. In the science, technology and medicine (STM) sector, the group publishes about 3,000 journals and 13,000 new books a year, as well as the largest STM eBook Collection worldwide.
Springer Nature's API provides metadata for 14 million online documents (e.g., journal articles, book chapters, protocols).

## API Documentation
Below is the link to relevant Springer API documentation used to develop this module:
* Springer API Documentation: https://dev.springernature.com/

## Software Design
The Springer module is separated into `helper_functions_springer.py` and `springer.py` files. The former contains functions that facilitate querying the Springer API tool. The latter file contains the module's main functions that intake user query input, call helper functions to query Springer's API, and extract metadata into a temporary list of dictionaries. Additinally, the latter file contains code that outputs said metadata in a CSV file if springer.py is run directly from the CLI.

## Search Results Limit & Module Issues
This module extracts a maximum of 50 results per query due to Springer's limiting search result count, but when there are more than 50 search results in a given query, another API query will be called until no results are missed.

## Springer Testing Instructions
1. Run main file: `python3 springer.py`
2. Enter query. You may include boolean operators. Sample query: `breast cancer AND algorithm AND treatment`
3. Run the same query on Springer's web browser tool (https://link.springer.com/).
4. Compare result count between API query and web query, and examine `springer.csv` to compare resulting publications collected in API tool with results showed on web browser. The latter step may be easier with more specific queries with fewer results.

