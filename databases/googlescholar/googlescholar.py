from serpapi import GoogleSearch
from urllib.parse import urlsplit, parse_qsl            # helps pagination process
import pandas, re, sys

# get API key
api_key_path = (str(sys.path[0])).rsplit("\\",1)[0].rsplit("\\",1)[0]
sys.path.insert(0, api_key_path)
from api_keys import google_scholar_api_key

# run search on SERP API's Google Scholar API
def scholar_search_results(query, google_scholar_api_key=google_scholar_api_key):

    search_parameters = {
        "api_key": google_scholar_api_key,              # API key
        "engine": "google_scholar",
        "q": query,
        "hl": "en",                                     # language of query
        "lr": "lang_en",                                # language of results
        "start": "0"                                    # begin scraping on first results page
    }

    # run search
    search = GoogleSearch(search_parameters)
    organic_results_data = []
    result_count = 0

    # extract data from each page
    loop_is_true = True
    while loop_is_true:
        results = search.get_dict()
        
        try:
          # extraction code & handing exceptions 
          for result in results["organic_results"]:

            result_type = result.get("type", "Unknown") 
            title = result.get("title", "Unknown")

            # for extracton of author(s), year, and source
            publication_info_summary = result["publication_info"]["summary"]
            pub_info = re.split("-+", publication_info_summary)

            # get publication year (only integer in publication_info_summary)
            year = re.search(r'\d+', publication_info_summary)
            year = int(year.group()) if year else "Unknown"

            # get author(s) into list and trim whitespace
            authors_with_spaces = re.split(",+", pub_info[0])
            authors = [author.replace("…","").strip() for author in authors_with_spaces]
            if bool(authors) == False: authors = "Unknown"

            # source
            source = (publication_info_summary.partition(str(year))[2]).replace('- ','')
            if bool(source) == False: source = "Unknown"

            # number of times cited by other articles on Google Scholar
            cited_by_count = result.get("inline_links", {}).get("cited_by", {}).get("total", {})
            if bool(cited_by_count) == False: cited_by_count = "Unknown"

            # publication link
            url = result.get("link", "Unknown")

            # get full text link if find file format pdf
            try:
              file_format = result["resources"][0]["file_format"]
            except: file_format = None

            pdf_file_url = None
            if file_format == "PDF":
              try:
                pdf_file_url = result["resources"][0]["link"]
              except: pdf_file_url = None

            if bool(pdf_file_url) == False: pdf_file_url = "Unknown"

            # abstract
            snippet = result.get("snippet", "Unknown")
            if bool(snippet) == True: snippet = "(GOOGLE SCHOLAR SNIPPET): " + snippet
    
            # append results to temp list as dictonary
            organic_results_data.append({
              "database": "Google Scholar",
              "result_type": result_type.strip(),
              "title": title.strip(),
              "year": year,
              "authors": authors,
              "source": source.strip(),
              "cited_by_count": cited_by_count,
              "url": url.strip(),
              "pdf_file_url": pdf_file_url,
              "abstract": snippet,
            })

            result_count += 1

        except TypeError as e:
          pass

        except KeyError as e:
          print("\t*** Google Scholar KeyError: " + str(e) + " ***")
          pass

        # if next page is present, update previous results to new page results
        try:
          if "next" in results["serpapi_pagination"]:
            search.params_dict.update(dict(parse_qsl(urlsplit(results["serpapi_pagination"]["next"]).query)))
          else: 
            loop_is_true = False
        except KeyError:
          loop_is_true = False
        
    print("\t"+ str(result_count) + " matching Google Scholar search results found... ")
    return organic_results_data


def main():
    query = input("\nPlease enter your search query:\t")
    pandas.DataFrame(data=scholar_search_results(query)).to_csv("googlescholar.csv", encoding="utf-8", index=False)
    print("\tGoogle Scholar search results saved!")


if __name__ == "__main__":
    main()