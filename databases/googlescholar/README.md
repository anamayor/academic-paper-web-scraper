# Google Scholar Module
This module extracts search results and their respective metadata from Google Scholar search results pages through SerpAPI's Google Scholar API.

## Googl Scholar Database & SerpAPI Overview
Google Scholar is a freely accessible web search engine that indexes the full text or metadata of scholarly literature across an array of publishing formats and disciplines. Google Scholar index includes peer-reviewed online academic journals and other publication formats.
SerpAPI's Google Scholar API allows users to scrape the search engine results page from a Google Scholar search query. The API is accessed through the following endpoint: /search?engine=google_scholar. Thank you SerpAPI for making this tool available for researchers.

## API Documentation
Below are links to relevant SerpAPI Google Scholar API documentation pages that were used in this project:
* User Manual 1: https://serpapi.com/google-scholar-api
* User Manual 2: https://serpapi.com/google-scholar-organic-results
* Sample blog 1: https://serpapi.com/blog/scrape-google-scholar-case-law-results-to-csv-with-python-and-serpapi/#prerequisites
* Sample blog 2: https://serpapi.com/blog/scrape-historic-google-scholar-results-using-python/

## Software Design
The Google Scholar module contains a single file `google_scholar.py`. The file contains code that facilitate querying the SerpAPI's Google Scholar API tool and extracting metadata from the results pages into a temporary list of dictionaries. Additinally, the contains a main function that outputs said metadata in a CSV file if google_scholar.py is run directly from the CLI.

## Search Results Limit & Module Issues
This module extracts a maximum of 1,000 results per query and per month due to SerpAPI limiting search result count for the free tier of their API key. This issue can be solved by contacting SerpAPI and asking for an API key with more results per month in exchange for mention on researchers' final publications.
In terms of issues, there are minor discrepancies for search results of multi-word queries when comparing API queries to web browser queries on Google Scholar (generally a difference of no more than 30 results).

## Google Scholar Testing Instructions
1. Run main file: `python3 google_scholar.py`
2. Enter query. You may include boolean operators. Sample query: `autonomic neuropathy great dane AND breeding protocol AND cancer AND heart disease`
3. Run the same query on Google Scholar's web browser tool (https://scholar.google.com/).
4. Compare result count between API query and web query, and examine `google_scholar.csv` to compare resulting publications collected in API tool with results showed on web browser. The latter step may be easier with more specific queries with fewer results.

