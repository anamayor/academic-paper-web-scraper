import arcas, pandas, urllib3                              
from helper_functions_ieee import mod_create_url_search
from api_keys import ieee_xplore_api_key


# disable insecure request warning (standard output print message)
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# run search on IEEE Xplore API
def ieee_xplore_search_results(query, ieee_xplore_api_key=ieee_xplore_api_key):

    results_data = []                       # temporary list to store extracted data

    try:
        api = arcas.Ieee()
        keywords = query                        # search query (search keyword in all fields)
        records_num = 200                       # max results 200 (limited by API)
        result_count = 0                        # result count
        results = []                        # list to store json_responses for result searches (allows to collect more than 50 results)
        search_parameters = api.parameters_fix(keyword=keywords, records=records_num) 

        # run search
        search_url = mod_create_url_search(search_parameters,ieee_xplore_api_key)
        request = api.make_request(search_url)
        root = api.get_root(request)
        json_response = api.parse(root)

        # get total results
        results_remaining = int(root["total_records"])

        # append first search results to json response list
        results.append(json_response)
        results_remaining -= records_num
        current_result_index = records_num + 1

        # if unaccounted results > 50 (limit per response on Springer API), run multiple searches
        while(results_remaining) > 0:
            # run search
            search_parameters = api.parameters_fix(keyword=keywords, records=records_num, start=current_result_index) 
            url = mod_create_url_search(search_parameters,ieee_xplore_api_key)
            root = api.get_root(request)
            json_response = api.parse(root)

            # append to response list
            results.append(json_response)
            results_remaining -= records_num
            current_result_index += records_num

        # store desired fields in temp list
        try: 
            for json_response in results:
                try:
                    for result in json_response:
                        article = api.to_dataframe(result)

                        result_type = result["content_type"]
                        title = result["title"]
                        year = result["publication_year"]
                        authors = article.author.unique()  
                        source = result["publication_title"]
                        
                        # get cited by count
                        cited_by_count = result["citing_paper_count"]
                        if cited_by_count == 0:
                            cited_by_count = "Unknown"
                        url = result["html_url"]
                        pdf_file_url = result["pdf_url"]
                        abstract = result["abstract"]

                        results_data.append({
                            "database": ["IEEE Xplore"],
                            "result_type": result_type,
                            "title": title,
                            "year": year,
                            "authors": authors,
                            "source": source,
                            "cited_by_count": cited_by_count,
                            "url": url,
                            "pdf_file_url": pdf_file_url,
                            "abstract": abstract,
                        })

                        result_count += 1

                except TypeError as e:
                    pass

                except KeyError as e:
                    print("\t*** IEEE Xplore KeyError: " + str(e) + " ***")
                    pass
                
        except Exception as e:
            pass

        print("\t"+ str(result_count) + " matching IEEE Xplore search results found... ")
        return results_data
    except arcas.tools.APIError as e:
        print("\t*** IEEE Xplore APIError (Missing/Invalid Key): " + str(e) + " ***")
        return results_data

def main():
    query = input("\nPlease enter your search query:\t")
    pandas.DataFrame(data=ieee_xplore_search_results(query)).to_csv("ieee_xplore.csv", encoding="utf-8", index=False)
    print("\tIEEE Xplore search results saved!")


if __name__ == "__main__":
    main()
    