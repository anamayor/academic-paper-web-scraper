# IEEE Explore Module
This module extracts search results and their respective metadata from IEEE Xplore's search result pages using IEEE's API and Nikoleta Glynatsi's open-source tool, Arcas.

## Database & IEEE API Overview
IEEE Xplore digital library is a research database for discovery and access to over 5 million journal articles, conference proceedings, technical standards, and related materials on computer science, electrical engineering and electronics, and allied fields. It contains material published mainly by the Institute of Electrical and Electronics Engineers (IEEE) and other partner publishers.
IEEE Xplore's API queries and retrieves metadata records including abstracts from IEEE Xplore search results.

## API Documentation
I used both IEEE Xplore's API user manual as well as Nikoleta Glynatsi's open-source Arcas tool. Arcas facilitates extracting metadata from a few databases overlapping with this project by streamlining API querying. Below is relevant documentation pertaining to IEEE's API and Arcas:
* IEEE Xplore API Documentation : https://arxiv.org/help/api/user-manual
* Arcas Documentation: 
    * User Manual: https://arcas.readthedocs.io/_/downloads/en/stable/pdf/
    * Github IEEE code: https://github.com/ArcasProject/Arcas/blob/master/src/arcas/IEEE/main.py

## Software Design
The IEEE module is separated into `helper_functions_ieee.py` and `ieee.py` files. The former contains functions that facilitate querying the IEEE API tool. The latter file contains the module's main functions that intake user query input, call helper functions to query IEEE's API, and extract metadata into a temporary list of dictionaries. Additinally, the latter file contains code that outputs said metadata in a CSV file if ieee.py is run directly from the CLI.

## Search Results Limit & Module Issues
This module extracts a maximum of 200 results per query due to IEEE's limiting search result count, but when there are more than 200 search results in a given query, another API query will be called until no results are missed.
In terms of issues, the only limiting factor for IEEE's API is that a query term can only contain a maximum of 10 words.

## IEEE Testing Instructions
1. Run main file: `python3 ieee.py`
2. Enter query. You may include boolean operators. Sample query: `greyhound AND racing`
3. Run the same query on IEEE Xplore's web browser tool (https://ieeexplore.ieee.org/).
4. Compare result count between API query and web query, and examine `ieee.csv` to compare resulting publications collected in API tool with results showed on web browser. The latter step may be easier with more specific queries with fewer results.

