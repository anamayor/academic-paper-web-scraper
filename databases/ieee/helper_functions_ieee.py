import sys                                  # to import API keys

# get API key
api_key_path = (str(sys.path[0])).rsplit("\\",1)[0].rsplit("\\",1)[0]
sys.path.insert(0, api_key_path)

# create url for IEEE Xplore API
def mod_create_url_search(parameters, ieee_xplore_api_key):
        url = 'https://ieeexploreapi.ieee.org/api/v1/search/articles?'
        url += parameters[0]
        for i in parameters[1:]:
            url += '&{}'.format(i)
        url += '&apikey={}'.format(ieee_xplore_api_key)
        return url