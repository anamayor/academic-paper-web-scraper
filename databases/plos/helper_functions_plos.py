import re

# determines which Plos journal a URL refers to to extract source and pdf URL
def get_plos_journal(id):
    journal = []
    if "journal.pbio." in id:
        journal.append("plosbiology")
        journal.append("pbio")
        journal.append("PLOS Biology")
    elif "journal.pclm." in id:
        journal.append("climate")
        journal.append("pclm")
        journal.append("PLOS Climate")
    elif "journal.pcbi." in id:
        journal.append("ploscompbiol")
        journal.append("pcbi")
        journal.append("PLOS Computational Biology")
    elif "journal.pdig." in id:
        journal.append("digitalhealth")
        journal.append("pdig")
        journal.append("PLOS Digital Health")
    elif "journal.pgen." in id:
        journal.append("plosgenetics")
        journal.append("pgen")
        journal.append("PLOS Genetics")
    elif "journal.pgph." in id:
        journal.append("globalpublichealth")
        journal.append("pgph")
        journal.append("PLOS Global Public Health")
    elif "journal.pmed." in id:
        journal.append("plosmedicine")
        journal.append("pmed")
        journal.append("PLOS Medicine")
    elif "journal.pntd." in id:
        journal.append("plosntds")
        journal.append("pntd")
        journal.append("PLOS Neglected Tropical Diseases")
    elif "journal.pone." in id:
        journal.append("plosone")
        journal.append("pone")
        journal.append("PLOS ONE")
    elif "journal.ppat." in id:
        journal.append("plospathogens")
        journal.append("ppat")
        journal.append("PLOS Pathogens")
    elif "journal.pstr." in id:
        journal.append("sustainabilitytransformation")
        journal.append("pstr")
        journal.append("PLOS Sustainability and Transformation")
    elif "journal.pwat." in id:
        journal.append("water")
        journal.append("pwat")
        journal.append("PLOS Water")
    return journal


# create url for IEEE Xplore API
def mod_create_url_search(query, num):
    url = "http://api.plos.org/search?q="
    query = query.replace(" ", "%20")
    url += query
    url += "&rows=" + str(num)
    return url

def author_string_to_list(str):
    str = str.replace("[","").replace("]","")
    str = str[1:(len(str)-1)]
    list_from_str = [s for s in re.split("' '", str)]
    return list_from_str