# PLOS Module
This module extracts search results and their respective metadata from PLOS's search result pages using PLOS's API and Nikoleta Glynatsi's open-source tool, Arcas.

## Database & PLOS API Overview
PLOS (Public Library of Science) is a nonprofit open-access science, technology, and medicine publisher with a library of open-access journals and other scientific literature under an open-content license. The publications are primarily funded by payments from the authors.
PLOS's API provides access to its article corpus and article metadata.

## API Documentation
I used both PLOS's API user manual as well as Nikoleta Glynatsi's open-source Arcas tool. Arcas facilitates extracting metadata from a few databases overlapping with this project by streamlining API querying. Below is relevant documentation pertaining to PLOS's API and Arcas:
* PLOS API Documentation : https://api.plos.org/text-and-data-mining/
* Arcas Documentation: 
    * User Manual: https://arcas.readthedocs.io/_/downloads/en/stable/pdf/
    * Github PLOS code: https://github.com/ArcasProject/Arcas/blob/master/src/arcas/PLOS/main.py

## Software Design
The PLOS module is separated into `helper_functions_plos.py` and `plos.py` files. The former contains functions that facilitate querying the PLOS API tool. The latter file contains the module's main functions that intake user query input, call helper functions to query PLOS's API, and extract metadata into a temporary list of dictionaries. Additinally, the latter file contains code that outputs said metadata in a CSV file if plos.py is run directly from the CLI.

## Search Results Limit & Module Issues
This module extracts a maximum of 7,200 results per query and per day due to PLOS's limiting search result count. Additionally, there is a limit of 10 results per minute and 300 per hour, as specified in PLOS's API documentation.
In terms of issues, the some discrepancies between API and web browser results counts, potentially due to discrepancies in searched journals.

## PLOS Testing Instructions
1. Run main file: `python3 plos.py`
2. Enter query. You may include boolean operators. Sample query: `cancer AND algorithm AND security AND patient privacy`
3. Run the same query on PLOS's web browser tool (https://journals.plos.org/plosone/).
4. Compare result count between API query and web query, and examine `plos.csv` to compare resulting publications collected in API tool with results showed on web browser. The latter step may be easier with more specific queries with fewer results.

