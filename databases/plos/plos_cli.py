import arcas, pandas, urllib3
from helper_functions_plos import get_plos_journal, mod_create_url_search, author_string_to_list

# disable insecure request warning (standard output print message)
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# run search on PLOS API
def plos_search_results(query):

    api = arcas.Plos()
    query = query                        # search query (search keyword in all fields)
    records_num = 7200                   # max results (API limits requests to 7200 requests/day, 300/h, 10/min)
    results_data = []                    # temporary list to store extracted data
    result_count = 0                     # result count

    # run search
    search_url = mod_create_url_search(query, records_num)
    request = api.make_request(search_url)
    root = api.get_root(request)
    raw_article = api.parse(root)

    # store desired fields in temp list
    try :
        for raw in raw_article:
            article = api.to_dataframe(raw)

            # find PLOS journal from id
            id = raw["id"]
            journal = get_plos_journal(id)

            result_type = raw["article_type"]
            title = (article.title.unique())[0]
            year = (article.date.unique())[0]

            # authors
            authors_string = str(article.author.unique())
            authors = author_string_to_list(authors_string)       

            # source from id
            try:
                source = journal[2]
            except IndexError:
                source = "Unknown" 
            
            cited_by_count = "Unknown"
            
            abstract = (article.abstract.unique())[0]
            url = (article.url.unique().tolist())[0]
            
            # pdf link from id and journal shorthand
            try:
                pdf_file_url = "https://journals.plos.org/" + journal[0] + "/article/file?id=" + id + "&type=printable"
            except IndexError:
                pdf_file_url = "Unknown"


            results_data.append({
                "database": "PLOS",
                "result_type": result_type,
                "title": title,
                "year": year,
                "authors": authors,
                "source": source,
                "cited_by_count": cited_by_count,
                "url": url,
                "pdf_file_url": pdf_file_url,
                "abstract": abstract,
            })

            result_count += 1

    except TypeError as e:
        pass

    except KeyError as e:
        print("\t*** PLOS KeyError: " + str(e) + " ***")
        pass

    print("\t"+ str(result_count) + " matching PLOS search results found... ")
    return results_data


def main():
    query = input("\nPlease enter your search query:\t")
    pandas.DataFrame(data=plos_search_results(query)).to_csv("plos.csv", encoding="utf-8", index=False)
    print("\tPLOS search results saved!")


if __name__ == "__main__":
    main()