from Bio import Entrez, Medline
import pandas, re, sys, urllib

# get Entrez email
entrez_email_path = (str(sys.path[0])).rsplit("\\",1)[0].rsplit("\\",1)[0]
sys.path.insert(0, entrez_email_path)
from api_keys import pubmed_email

# run search on Pubmed API
def pubmed_search_results(query):
    results_data = []                       # temporary list to store extracted data

    try:
        Entrez.email = pubmed_email         # to be contacted in the case of a problem (should include)
        keywords = query                    # search query
        result_count = 0                    # result count

        # run search & get result IDs
        handle = Entrez.esearch(db="pubmed", term=keywords, retmax=1000000)
        record = Entrez.read(handle)
        idlist = record["IdList"]

        # extract information from ID list
        handle = Entrez.efetch(db="pubmed", id=idlist, rettype="medline")
        records = Medline.parse(handle)
        records = list(records)

        # store desired fields (if not found, store as "unknown")
        try:
            for record in records:

                result_type = record.get("JT", "Unknown")
                title = record.get("TI", "Unknown")

                # get year from date
                date = record.get("DP", "Unknown")
                date_info = re.split(" ", date)
                year = date_info[0]

                authors = record.get("AU", "Unknown")
                source = record.get("TA", "Unknown")
                cited_by_count = "Unknown"

                # build pubmed url from PMID
                pmid = record.get("PMID", "Unknown")
                if pmid == "Unknown":
                    url = "Unknown"
                else:
                    url = "https://pubmed.ncbi.nlm.nih.gov/" + pmid + "/"

                # build full text url from pmc
                pmc = record.get("PMC", "Unknown")
                if pmc == "Unknown":
                    pdf_file_url = "Unknown"
                else:
                    pdf_file_url = "https://www.ncbi.nlm.nih.gov/pmc/articles/" + pmc + "/pdf/"

                abstract = record.get("AB", "Unknown")

                # append data to results list and increment result count if article title is defined at minimum
                if (title != "Unknown"):
                    results_data.append({
                        "database": "PubMed",
                        "result_type": result_type,
                        "title": title.strip(),
                        "year": year,
                        "authors": authors,
                        "source": source.strip(),
                        "cited_by_count": cited_by_count,
                        "url": url,
                        "pdf_file_url": pdf_file_url,
                        "abstract": abstract,
                    })

                    result_count += 1

        except TypeError as e:
            pass

        except KeyError as e:
            print("\t*** PubMed KeyError: " + str(e) + " ***")
            pass
        
        print("\t"+ str(result_count) + " matching PubMed search results found... ")
        return results_data
    except urllib.error.HTTPError as e:
        print("\t*** PubMed HTTPError: " + str(e) + " ***")
        return results_data



def main():
    query = input("\nPlease enter your search query:\t")
    pandas.DataFrame(data=pubmed_search_results(query)).to_csv("pubmed.csv", encoding="utf-8", index=False)
    print("\tPubMed search results saved!")


if __name__ == "__main__":
    main()
