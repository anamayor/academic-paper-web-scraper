# PubMed Module
This module extracts search results and their respective metadata from PubMed search results pages through MEDLINE's API tool via the biopython library.

## PubMed Database & MEDLINE API Overview
PubMed is a free search engine accessing primarily the MEDLINE database of references and abstracts on life sciences and biomedical topics. The United States National Library of Medicine (NLM) at the National Institutes of Health maintain the database as part of the Entrez system of information retrieval.
Biopython's MEDLINE module contains code to work with Medline from the NCBI, which can be used to extract metadata from PubMed's search results page.

## API Documentation
Below are links to relevant Biopython information regarding querying PubMed's result page:
* Biopython Documentation: https://biopython-tutorial.readthedocs.io/en/latest/notebooks/09%20-%20Accessing%20NCBIs%20Entrez%20databases.html#Using-the-history-and-WebEnv
* YouTube Tutorial: https://www.youtube.com/watch?v=aETx4MyXukk

## Software Design
The PubMed module contains a single file `pubmed.py`. The file contains code that facilitate querying the Biopython's MEDLINE tool and extracting metadata from the PubMed results pages into a temporary list of dictionaries. Additinally, the contains a main function that outputs said metadata in a CSV file if pubmed.py is run directly from the CLI.

## Search Results Limit & Module Issues
There are no maximum number of results for this module, but Biopython's MEDLINE module is limited to extract 3 results per second.
In terms of issues, there are minor discrepancies for search results of multi-word queries when comparing API queries to web browser queries on PubMed (generally a difference of no more than 30 results). Discrepancies for single word queries are slightly larger, but excessive.

## PubMed Testing Instructions
1. Run main file: `python3 pubmed.py`
2. Enter query. You may include boolean operators. Sample query: `autonomic neuropathy AND cancer AND dog`
3. Run the same query on PubMed's web browser tool (https://pubmed.ncbi.nlm.nih.gov/).
4. Compare result count between API query and web query, and examine `pubmed.csv` to compare resulting publications collected in API tool with results showed on web browser. The latter step may be easier with more specific queries with fewer results.

