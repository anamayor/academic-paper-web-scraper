# arXiv Module
This module extracts search results and their respective metadata from arXiv's search result pages using arXiv's API and Nikoleta Glynatsi's open-source tool, Arcas.

## arXiv API & Database Overview
The Cornell University e-print arXiv, hosted at arXiv.org, is a document submission and retrieval system that is heavily used by the physics, mathematics and computer science communities. It has become the primary means of communicating cutting-edge manuscripts on current and ongoing research. The open-access arXiv e-print repository is available worldwide, and presents no entry barriers to readers, thus facilitating scholarly communication.
The purpose of the arXiv API is to allow programmatic access to the arXiv's e-print content and metadata. The goal of the interface is to facilitate new and creative use of the the vast body of material on the arXiv by providing a low barrier to entry for application developers. Thank you to arXiv for use of its open access interoperability.

## API Documentation
I used both arXiv's API user manual as well as Nikoleta Glynatsi's open-source tool, Arcas. Arcas facilitates extracting metadata from a few databases overlapping with this project by streamlining API querying. Below is relevant documentation pertaining to arXiv's API and Arcas:
* arXiv API:
    * API Basics: https://arxiv.org/help/api/basics
    * User Manual: https://arxiv.org/help/api/user-manual
    * Terms of Use: https://arxiv.org/help/api/tou
* Arcas Documentation: 
    * User Manual: https://arcas.readthedocs.io/_/downloads/en/stable/pdf/
    * Github arXiv code: https://github.com/ArcasProject/Arcas/blob/master/src/arcas/arXiv/main.py

## Software Design
The arXiv module is separated into `helper_functions_arxiv.py` and `arxiv.py` files. The former contains functions that facilitate querying the arXiv API tool and turning broad queries into literal ones, as explained in the "Query modification: Literal queries" section of this README. The latter file contains the module's main functions that intake user query input, call helper functions to query arXiv's API, and extract metadata into a temporary list of dictionaries. Additinally, the latter file contains code that outputs said metadata in a CSV file if arxiv.py is run directly from the CLI.

## Search Results Limit & Module Issues
This module extracts a maximum of 10,000 results per query due to arXiv limiting search result count.
In terms of issues, there are minor discrepancies for search results of single word queries when comparing API queries to web browser queries on arXiv (generally a difference of no more than 10 results).

## Query Modification: Literal queries
When testing API query results, I noticed that multi-word queries resulted in significantly more results than the same queries on arXiv's web browser search tool. This issue persisted when API queries were modified to have parentheses surrounding multi-word queries, as seen below:
* `breast cancer` had over 10,000 results on API search tool, but only 1,017 on web browser search tool
* `breast cancer` was queried as `(breast cancer)` on API search tool to try to mend this issue, but it persisted.
* `breast cancer AND patient treatment` queried as `(breast cancer) AND (patient treatment)` on API search tool to try to mend this issue, but it persisted.
To improve this issue and improve the accuracy of the API search tool, queries with more than 2 words (e.g. "breast cancer") have to be made as literal queries surrounded by double quotes. This is justified by fact that Arxiv is in itself a relatively small database compared to larger databases in this project, and it is generally not greatly affected by literal queries as opposed to broad queries. Broad queries get turned into literal queries when `literal_query()` is called (in `helper_functions_arxiv.py`) Below is an example of this query of this query modification:
* `breast cancer AND patient treatment` queried as `"breast cancer" AND "patient treatment"` on API search tool.

## arXiv Testing Instructions
1. Run main file: `python3 arxiv.py`
2. Enter query. You may include boolean operators. Sample query: `breast cancer AND algorithm AND treatment`
3. Run the same query on arXiv's web browser tool (https://arxiv.org/search/).
4. Compare result count between API query and web query, and examine `arxiv.csv` to compare resulting publications collected in API tool with results showed on web browser. The latter step may be easier with more specific queries with fewer results.

