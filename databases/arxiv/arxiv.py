import arcas, pandas
from .helper_functions_arxiv import mod_create_url_search, parse

def arxiv_search_results(query):

    api = arcas.Arxiv()
    keywords = query                        # search query (search keyword in all fields)
    records_num = 10000                     # max results 10,000 (limited by API)
    results_data = []                       # temporary list to store extracted data
    result_count = 0                        # result count

    # run search
    url = mod_create_url_search(keywords, records_num)
    request = api.make_request(url)
    root = api.get_root(request)
    raw_article = parse(api, root)

    # store desired data fields in temp list
    try: 
        for raw in raw_article:
            article = api.to_dataframe(raw)

            result_type = "arXiv e-print"
            title = (article.title.unique())[0]
            year = (article.date.unique())[0]
            authors = article.author.unique()        
            source = (article.journal.unique())[0]
            cited_by_count = "Unknown"
            url = (article.url.unique())[0]
            pdf_file_url = (url.replace("/abs/","/pdf/")) + ".pdf"
            abstract = (article.abstract.unique())[0]            

            results_data.append({
                "database": ["arXiv"],
                "result_type": result_type,
                "title": title,
                "year": year,
                "authors": authors,
                "source": source,
                "cited_by_count": cited_by_count,
                "url": url,
                "pdf_file_url": pdf_file_url,
                "abstract": abstract,
            })

            result_count += 1

    except TypeError as e:
        pass

    except KeyError as e:
        print("\t*** arXiv KeyError: " + str(e) + " ***")
        pass

    print("\t"+ str(result_count) + " matching arXiv search results found... ")
    return results_data
    
    
def main():
    query = input("\nPlease enter your search query:\t")
    pandas.DataFrame(data=arxiv_search_results(query)).to_csv("arxiv.csv", encoding="utf-8", index=False)
    print("\tarXiv search results saved!")


if __name__ == "__main__":
    main()