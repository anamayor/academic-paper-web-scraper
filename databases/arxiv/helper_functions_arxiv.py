# returns only positive elements of list
def positive(lst):
    return [x for x in lst if x > 0] or None


# convert arxiv query into literal query
# e.g. query 'breast cancer AND life expectancy' becomes '"breast cancer" AND "life expectancy"'
def literal_query(query):

    # if there are boolean operators, split into string before first occurrence plus first boolean operator and rest (recursive case)
    if any(boolean_operator in query for boolean_operator in ['AND', 'OR', 'NOT', 'AND NOT']):

        # find which boolean operator appears first
        and_index = query.find('AND')
        or_index = query.find('OR')
        not_index = query.find('NOT')
        and_not_index = query.find('AND NOT')
        min_boolean_indexes = min(positive([and_index, or_index, not_index, and_not_index]))

        # if 'AND' or 'AND NOT' first
        if and_index == min_boolean_indexes:
            # if 'AND NOT' is first operator
            if and_index == and_not_index:
                split_query = query.split(' AND NOT ', 1)
                return literal_query(split_query[0]) + ' AND NOT ' + literal_query(split_query[1])
            else:
                split_query = query.split(' AND ', 1)
                return literal_query(split_query[0]) + ' AND ' + literal_query(split_query[1])

        # if 'OR' first
        elif or_index == min_boolean_indexes:
            split_query = query.split(' OR ', 1)
            return literal_query(split_query[0]) + ' OR ' + literal_query(split_query[1])

        # if 'NOT' first
        elif not_index == min_boolean_indexes:
            split_query = query.split(' NOT ', 1)
            return literal_query(split_query[0]) + ' NOT ' + literal_query(split_query[1])
        
    # if there are no boolean operators, simply add quotes around query (base case)
    else:
        query = "%22" + query + "%22"           # %22 is a quotation mark
        return query


# create URL to query Arxiv API combining search query and other parameters
def mod_create_url_search(query, records_num):

        url = "http://export.arxiv.org/api/query?search_query="

        # add query
        query = query.replace('"', '')          # remove any initial quotes
        query = literal_query(query)            # make query literal (add quotes except for boolean operators)
        query = query.replace(" ", "+")         # replace spaced for "+" to abide by API query rules
        url += ""+ query
        
        # add other search parameters
        url += "&searchtype=all"                
        url += "&max_results=" + str(records_num)
        url+= "&start=0"

        return url

def parse(api_self, root):
    """Removing unwanted branches."""
    branches = list(root)
    raw_articles = []
    for record in branches:
        if 'entry' in record.tag:
            raw_articles.append(api_self.xml_to_dict(record))
    if not raw_articles:
        raw_articles = False
    return raw_articles