import arcas    # documentation: https://github.com/ArcasProject/Arcas/blob/master/src/arcas/arXiv/main.py
import pandas   # to save data into CSV file

def nature_search_results():
    api = arcas.Nature()
    print("Extracting Nature search results... ")

    # search query (search for keyword in all fields) & number of results limit (keep high)
    query = "greyhound"     # TODO: does not support boolean logic queries but is accurate otherwise
    records_num = 10000
    parameters = api.parameters_fix(keyword=query, records=records_num)

    # temporary list to store extracted data
    results_data = []  

    # run search
    url = api.create_url_search(parameters)
    request = api.make_request(url)
    root = api.get_root(request)
    raw_article = api.parse(root)

    # result count
    result_count = 0

    # store desired fields
    for raw in raw_article:
        article = api.to_dataframe(raw)

        # authors
        authors = article.author.unique()        
        
        # extract object from array of single objects to preserve data format (title, year, url, abstract)
        title = (article.title.unique())[0]
        year = (article.date.unique())[0]
        abstract = (article.abstract.unique())[0]
        
        # get URL in desired format
        URL = (article.url.unique().tolist())[0]
        print(URL)
        
        # find PLOS journal from URL
        journal = article.journal.unique()
        print(journal)

        category = article.category.unique()
        print(category)

        # get article id from URL and journal
        
        # build full text url
        
        # source from URL
        

        results_data.append({
            "result_type": "Nature results",
            "title": title,
            "year": year,
            "authors": authors,
            "source": journal,
            "cited_by_count": "Unknown",
            "URL": URL,
            #"pdf_file_link": full_text_URL,
            "abstract": abstract,
          })

        # increment result count
        result_count += 1

    print(str(result_count) + " matching Nature search results found... ")
    
    return results_data



def save_nature_search_results_to_csv():
    pandas.DataFrame(data=nature_search_results()).to_csv("nature.csv", encoding="utf-8", index=False)
    print("Nature search results saved!")



if __name__ == "__main__":
    save_nature_search_results_to_csv()