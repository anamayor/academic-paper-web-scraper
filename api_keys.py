# GOOGLE SCHOLAR
# get key by signing up for free account at https://serpapi.com/users/sign_up
google_scholar_api_key = "9b4f939011fb176a8bcc92084313886a33e68d7ab02103782a980d1162d594de"

# IEEE XPLORE
# get key for free at https://developer.ieee.org/Quick_Start_Guide
ieee_xplore_api_key = "gekzudf7mruffxd28ekmk85e"

# SPRINGER
# get key for free at https://dev.springernature.com/signup?cannot_be_converted_to_param
springer_api_key = "315c5967e0fe2d2abb592cefd146fb37"

# PUBMED
# highly recommended for best functionality - give email to receive information in the case that there are issues with PubMed search 
pubmed_email = "ana.mayordomo@du.edu"

# Other databases do not need API keys

