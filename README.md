# Academic Database Publication Web Scraper for Researchers
## Overview
My name is Ana Mayordomo, and I am an undergraduate alumni from the University of Denver with BS degrees in Computer Science and Mathematics, and BSBA degree in Marketing. From April to November of 2022, I developed an academic database web scraper that queries significant research databases using existing APIs, extracts metadata from results in each database, and downloads available PDF articles of said results. I successfully defended my undergraduate thesis under the project name "Research Crawler: Academic Database Web Scraper for Researchers to Facilitate Systematization of Knowledge" on the 18th of November of 2022, with Dr. Sanchari Das and Dr. Daniel Pittman as part of my evaluation committee.

I worked under the guidance of Dr. Sanchari Das. The web scraper will be used in future research projects and publications from Dr. Das’ Inclusive Security and Privacy-focused Innovative Research in Information Technology (InSPIRIT) Research Laboratory. Although this tool was developed for a computer science research team, it may be used to gather information on existing research in other academic fields, including business, humanities, applied and natural sciences, and social sciences. Moreover, this web scraper can further be expanded to include additional relevant databases as well as bbeing integrated into existing multi-database query tools like OpenAlex, adding to its potential relevance as a research tool in varied academic disciplines. 

## Software Design
Research Crawler is a Python program that facilitates scraping academic journal publication metadata from academic databases as well as downloading the resulting PDFs from said search. This scraper gathers data based on user queries and returns a CSV file with organized publication metadata excluding duplicate results, as well as the PDFs of publications collected in said search. Scraped fields include database name, result types, title, year, authors, source, number of papers that cited the source, HTML URL, PDF URL, and abstract.  

This tool may be run from both the command line interface (CLI) as well as hosted on a server. To run from the command line, follow instructions in "Testing & Usage Instructions from CLI" section. This repository also code for the Research Crawler's UI that was be hosted on a DU Ubuntu server for accessibility and ease of use. See instructions under "Testing & Usage Instructions for Web App." Note that the app must be running on a server for it to be accessible, and that the code in this repository is specific to a server running Ubuntu OS 20.04 and Python 3.8 by default.

## Supported Databases
See documentation for each of these databases in their respective module's `README.md` under the `\databases\` directory.
1. Google Scholar
2. PubMed
3. PLOS
4. IEEE
5. Springer
6. ArXiv

## Future Databases
1. ACM
2. ResearchGate
3. SSRN
4. Other relevant databases with and without APIs
    * https://libguides.gc.cuny.edu/c.php?g=405353&p=4857784
    * https://guides.lib.berkeley.edu/information-studies/apis
5. Multi-database query tools resulting from Microsoft Academic's sunset in 2022 (i.e. Open Alex and others)

## Issues & Future Work
1. UI page where user can follow search process (results per database), opt to download PDFs, get progress on downloadd process, then redirect back to search
2. Springer cannot download pdfs bc not logged in? --> try this https://pybit.es/articles/requests-session/
3. PDFs from PubMed downloaded as html, need to convert to pdf
4. Google Scholar sometimes has BOOKS. Take forever to download. Filter out somehow.
5. Remove Arcas dependencies (deprecated or not maintained)
6. ACM chrome extension issues
7. Special characters on pdfs (', umlauts)
8. Key format in API index page for ease of use.

## Testing & Usage Instructions from CLI
1. Clone this repository onto your local machine.
2. Install requirements: `pip install -r requirements.txt`
3. Open `api_keys.py` and follow comment instructions to get your own API keys and paste them on the file.
4. Run `main_cli.py` on the command line: `python3 main_cli.py`
5. Input your desired query WITHOUT quotes unless explicitly looking for literal phrases. Boolen operators are supported.
6. Review results in `\search results\` folder. This program will create a folder named after your query and include the CSV results summary and the PDFs associated with it if you choose to download them.
7. You may test individual databases by navigating to `\databases\` and following testing instructions in each database's folder README.md.

## Testing & Usage Instructions for Web App
1. Connect to the DU VPN if you are not in the DU network.
2. Run `ping 130.253.8.34` on your CLI to test that the web app is running on the server.
3. Search http://130.253.8.34:5000/ on your browser.
4. Enter your API keys in the initial page. If needed, follow instructions on the page to get your API keys.
5. Navigate to http://130.253.8.34:5000/search through your browser search bar, app navigation bar, or home page link.
6. Input your desired query WITHOUT quotes unless explicitly looking for literal phrases. Boolen operators are supported.
7. Wait for .zip folder to be collected. This folder will include .csv file with collected result metadata as well as pdfs that were downloaded.
