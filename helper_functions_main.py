import requests

# returns number of pdf links in a search results dataframe from all databases
def get_num_pdf_links(dataframe):
    num_links = 0

    try:
        for url in dataframe["pdf_file_url"]:
            if url is not "Unknown":
                num_links += 1
    except KeyError:
        pass

    return num_links

# returns whether or not user wants to download pdf links in dataframe
def get_user_download_input(num_links):
    user_input = input("\n" + str(num_links) + " PDF links collected. Do you wish to download articles with available pdf links? (Y/n)\t")

    while user_input not in ['y', 'Y', 'n', 'N']:
        print("Invalid choice")
        user_input = input("\nDo you with to download articles with available pdf links? (Y/n)\t")
    
    return user_input

# downloads PDFs in dataframe of database search results if possible (if error occurs, pdf skipped and download continues)
def download_pdfs(dataframe, folder_name):
    df_index = 0
    downloaded_pdfs = 0
    
    if dataframe is not None:   # check dataframe not empty
        try:
            for url in dataframe["pdf_file_url"]:  # for each pdf url, download pdf when possible
                df_index += 1
                if url != "Unknown":
                    try:
                        response = requests.get(url)
                        pdf = open(folder_name + "/" + str(df_index)+".pdf", 'wb')
                        pdf.write(response.content)
                        pdf.close()
                        downloaded_pdfs += 1
                    
                    except requests.RequestException:
                        pass

        except KeyError:
          pass
    
    return downloaded_pdfs