import sys, pandas, os, urllib
from helper_functions_main import get_num_pdf_links, get_user_download_input, download_pdfs

# insert paths to database scraping files so can import scraping functions
parent = str(sys.path[0])
paths = [(parent + r"\databases\arXiv"), (parent + r"\databases\googlescholar"), (parent + r"\databases\ieee"), (parent + r"\databases\plos"), (parent + r"\databases\pubmed"), (parent + r"\databases\springer")]
for path in paths:
    sys.path.insert(0, path)

# import scraping functions
from googlescholar import scholar_search_results
from pubmed import pubmed_search_results
from plos_cli import plos_search_results
from ieeexplore_cli import ieee_xplore_search_results
from springer_cli import springer_search_results
from arxiv_cli import arxiv_search_results

# search databases, create search results folder, create query folder, store results in csv, download pdfs if asked to do so
def main(query): 
    
    print("\nSearching all databases for '" + query + "'")

    # declare dataframe for each database
    scholar_df = None
    pubmed_df = None
    plos_df = None
    ieee_df = None
    springer_df = None
    arxiv_df = None

    # run query in each database
    scholar_df = pandas.DataFrame(scholar_search_results(query))
    pubmed_df = pandas.DataFrame(data = pubmed_search_results(query))

    plos_df = pandas.DataFrame(data = plos_search_results(query))
    ieee_df = pandas.DataFrame(data = ieee_xplore_search_results(query))
    springer_df = pandas.DataFrame(data = springer_search_results(query))
    arxiv_df = pandas.DataFrame(data = arxiv_search_results(query))
    
    # combine results into one dataframe, remove duplicates (same title & year), get number of PDF links collected
    frames = [scholar_df, pubmed_df, plos_df, ieee_df, springer_df, arxiv_df]
    result = pandas.concat(frames)
    result.drop_duplicates(subset=['title', 'year'])
    pdf_links = get_num_pdf_links(result)

    # name folder and csv names (limit to 30 chars to avoid issues with names that are too long, so 26 because of file extension for file name)
    # if have to cut file name length, last char is _ before extension to indicate it is shortened 
    base_query_name = (query.replace(" ", "_")).replace("'", "").replace('"', '')
    if len(base_query_name)>30:
        base_query_name = base_query_name[0:28] + "_"
    folder_name = "search results/"+ base_query_name

    if len(base_query_name)>26:
        base_query_name = base_query_name[0:24] + "_"
    csv_name = base_query_name + ".csv"

    # create folder to save results & pdfs if it does not exist 
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)
    
    # create csv file with unique results
    result.to_csv(folder_name+"/"+csv_name, encoding="utf-8", index=False)
    print("\n" + str(len(result)) + " unique search results saved to " + csv_name)

    # get user download choice & if user chooses, download pdfs and output message reflecting decision
    download = get_user_download_input(pdf_links)
    if download == 'y':
        print("\n" + "Downloading PDFs...")
        num_pdfs = download_pdfs(result, folder_name)
        print("\n" + str(num_pdfs) + " PDF document(s) saved in query search results folder\n")
    else: print("\nNo PDF documents saved\n")


if __name__=="__main__":
    query = input("\nPlease enter your search query:\t\t")
    main(query)