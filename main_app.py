# https://stackoverflow.com/questions/62823361/download-dataframe-as-csv-from-flask
import sys, pandas, io, zipfile, requests
from flask import Flask, render_template, request,  session, send_file,redirect
from flask_session import Session


# insert paths to database scraping files so can import scraping functions
parent = str(sys.path[0])
paths = [(parent + r"\databases\arXiv"), (parent + r"\databases\googlescholar"), (parent + r"\databases\ieee"), (parent + r"\databases\plos"), (parent + r"\databases\pubmed"), (parent + r"\databases\springer")]
for path in paths:
    sys.path.insert(0, path)

# import scraping functions
from databases.googlescholar.googlescholar import scholar_search_results
from databases.pubmed.pubmed import pubmed_search_results
from databases.plos.plos import plos_search_results
from databases.ieee.ieeexplore import ieee_xplore_search_results
from databases.springer.springer import springer_search_results
from databases.arxiv.arxiv import arxiv_search_results

app = Flask(__name__)


#create session object
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)


# Set the secret key to some random bytes and keep it secret.
# A secret key is needed in order to use sessions.
app.secret_key = b"_j'yXdW7.63}}b7"

@app.route("/", methods=["GET", "POST"])
def render_main():

    #if its a post method, add api keys to session and send home page 
    if request.method == "POST":
        session["google_scholar_api_key"]=request.form.get("google_scholar_api_key")
        session["ieee_xplore_api_key"]=request.form.get("ieee_xplore_api_key")
        session["springer_api_key"]=request.form.get("springer_api_key")
        print(session["google_scholar_api_key"],session["ieee_xplore_api_key"],session["springer_api_key"])
        return redirect('/home')

    #if its a get method
    if request.method == "GET":
        condition=session.get('google_scholar_api_key') and  session.get('google_scholar_api_key') and session.get('springer_api_key')
        if condition:
            return redirect('/home')
        return render_template('loginApi.html')

@app.route("/home")
def render_home():
    #check to  see the Api keys are added to session
    condition=session.get('google_scholar_api_key') and  session.get('google_scholar_api_key') and session.get('springer_api_key')
    if not condition:
        return redirect('/')

    #If the all the keys are in session
    return render_template('home.html')

@app.route("/logout")
def logout():
    session["google_scholar_api_key"]=None
    session["ieee_xplore_api_key"]=None
    session["springer_api_key"]=None
    session.clear()
    return redirect("/")



@app.route("/search")
def render_search():
    condition=session.get('google_scholar_api_key') and  session.get('google_scholar_api_key') and session.get('springer_api_key')
    if not condition:
        return redirect('/')

    return render_template("search.html")


@app.route("/search_results", methods=["GET", "POST"])
def render_search_results():
    condition=session.get('google_scholar_api_key') and  session.get('google_scholar_api_key') and session.get('springer_api_key')
    if not condition:
        return redirect('/')

    try: 
        # get metadata
        user_query = str(request.args['query'])
        df = search(user_query)

        # store metadata in CSV
        csv = df.to_csv(index=False, header=True, encoding="utf-8")

        # name zip and csv names (limit to 30 chars to avoid issues with names that are too long, so 26 because of file extension for file name)
        # if have to cut file name length, last char is _ before extension to indicate it is shortened 
        base_query_name = ((user_query.replace(" ", "_")).replace("'", "")).replace('"', '')
        if len(base_query_name)>30:
            base_query_name = base_query_name[0:28] + "_"
        zip_name = base_query_name + ".zip"

        if len(base_query_name)>26:
            base_query_name = base_query_name[0:24] + "_"
        csv_name = base_query_name + ".csv"

        # get csv content into temp list
        temp_files = [(csv_name, csv)]

        # add pdfs to temp list
        df_index = 0
        try:
            for url in df["pdf_file_url"]:  # for each pdf url, download pdf when possible
                df_index += 1
                if url != "Unknown":
                    try:
                        response = requests.get(url)
                        temp_files.append((str(df_index)+".pdf", response.content))                    
                    except requests.RequestException:
                        pass

        except KeyError:
          pass

        data = io.BytesIO()
        with zipfile.ZipFile(data, mode='w') as z:
            for name, content in temp_files:
                z.writestr(name, content)
            z.close()

        data.seek(0)
        return send_file(data, mimetype='application/zip', as_attachment=True, download_name=zip_name)

    except ValueError:
        return "Sorry: something went wrong."


def search(query): 
    ''' Search databases, return dataframe results'''

    # run query in each database
    #scholar_df = None
    scholar_df = pandas.DataFrame(scholar_search_results(query,session["google_scholar_api_key"])) #scholar_df = None   #
    pubmed_df = pandas.DataFrame(data = pubmed_search_results(query))
    plos_df = pandas.DataFrame(data = plos_search_results(query))
    ieee_df = pandas.DataFrame(data = ieee_xplore_search_results(query,session["ieee_xplore_api_key"] ))
    springer_df = pandas.DataFrame(data = springer_search_results(query,session["springer_api_key"]))
    arxiv_df = pandas.DataFrame(data = arxiv_search_results(query))
    
    # combine results into one dataframe, remove duplicates (same title & year), get number of PDF links collected
    frames = [scholar_df, pubmed_df, plos_df, ieee_df, springer_df, arxiv_df]
    result = pandas.concat(frames)
    result.drop_duplicates(subset=['title', 'year'])

    # return dataframe results
    return result


def download_pdfs(dataframe, folder_name):
    ''' Downloads PDFs in dataframe of database search results if possible (if error occurs, pdf skipped and download continues)'''
    df_index = 0
    downloaded_pdfs = 0
    
    print("\n" + "Downloading PDFs...")

    if dataframe is not None:   # check dataframe not empty
        try:
            for url in dataframe["pdf_file_url"]:  # for each pdf url, download pdf when possible
                df_index += 1
                if url != "Unknown":
                    try:
                        response = requests.get(url)
                        pdf = open(folder_name + "/" + str(df_index)+".pdf", 'wb')
                        pdf.write(response.content)
                        pdf.close()
                        downloaded_pdfs += 1
                    
                    except requests.RequestException:
                        pass

        except KeyError:
          pass
    
    print("\n" + str(downloaded_pdfs) + " PDF document(s) saved in zip folder\n")    
    return downloaded_pdfs


if __name__=="__main__":
    app.run(port=5000, debug=True)
    #app.run(host="130.253.8.34")